#include<iostream>
#include"Static.h"
#include<Windows.h>

typedef void(_stdcall *HelloFromDll1)();

extern "C" void _cdecl HelloFromDll2();

using namespace std;

void main()
{
	//static
	StaticClass::Static::HelloFromStatic();
	//run-time
	HINSTANCE hModule = LoadLibrary(TEXT("DLL1.dll"));
	HelloFromDll1 HelloOnStart = (HelloFromDll1)GetProcAddress(hModule, "HelloFromDll1");
	if (HelloOnStart)
	{
		HelloOnStart();
	}
	//load-time
	HelloFromDll2();

}