#include <iostream>
#include <fstream>
#include <string>
#include <regex>
#include <map>
#include <stdlib.h>
#include <Windows.h>
using namespace std;

void main() {
	map<string, int> mapCarte;
	map<string, int>::iterator it = mapCarte.begin();
	map<string, int>::iterator end = mapCarte.end();
	int max = 0;
	int mapConstructBegin, mapConstructEnd, wordExistBegin, wordExistEnd, wordApearBegin, wordApearEnd;
	string curentLine;
	string line;
	string arg_max;
	ifstream myfile("Carte.txt");
	ofstream myoutfile("CarteOut.txt");
	;

	//Citirea fisierului si crearea map-ului.
	if (myfile.is_open())
	{
		mapConstructBegin = GetTickCount();
		while (getline(myfile, line))
		{
			regex rgx("[\n\\.,\\s!;?:\"]+");
			sregex_token_iterator iter(line.begin(), line.end(), rgx, -1);
			sregex_token_iterator end;
			for (; iter != end; ++iter)
			{
				if (mapCarte.count(*iter) != 0)
				{
					mapCarte[*iter]++;
				}
				else
				{
					mapCarte.insert(pair<string, int>(*iter, 1));
				}

			}
		}

		mapConstructEnd = GetTickCount();
		cout << "Timpul de construire a map-ului: " << (mapConstructEnd - mapConstructBegin) / 1000 << endl;


		wordExistBegin = GetTickCount();
		wordExistEnd = GetTickCount();
		string test = "newsletter";
		for (it = mapCarte.begin(); it != mapCarte.end(); ++it)
		{
			if (it->first.compare(test) == 0)
			{
				cout << "Exista" << endl;
				wordExistEnd = GetTickCount();
				break;
			}
		}

		cout << "Timpul de cautare a unui cuvant: " << wordExistEnd - wordExistBegin << endl;

		cout << "Lista cuvintelor ce apar de mai mult de 500 de ori: " << endl;

		for (it = mapCarte.begin(); it != mapCarte.end(); ++it)
		{
			if (it->second > 500)
			{
				cout << it->first << " => " << it->second << endl;
			}

			if (it->second > max)
			{
				arg_max = it->first;
				max = it->second;
			}
		}

		cout << "Cuvantul cel mai des intalnit: ";
		cout << arg_max << " => " << max << endl;

		myfile.close();
	}
	else
	{
		cout << "Unable to open file";
	}

	/*while (getline(myfile, line))
	myoutfile << line << endl;*/

}