#include"DeadCode.h"
#include <iostream>
#include <Vector>
#include <fstream>
using namespace std;
#define _CRT_SECURE_NO_WARNINGS
void main()
{
	//deadcode bug:
	DeadCode a(3, 4);
	std::cout << a.suma()<<std::endl;


	//out of bounds bug:
	int v[11];
	for (int i = 0; i < 10; i++)
	{
		v[i] = i;
		cout << v[i] << endl;
	}
	


	//inexisting file and memory leak (no f.close()):

	int b;
	ifstream f("file.txt");
	f >> b;
	cout << b<<endl;
	f.close();

	//Bad allocation of memory:
	int **matrix=NULL;
	int n = 3,m=3;
	matrix = new int *[n];
	for (int i = 0; i < n; i++)
	{
		matrix[i] = new int[m];
	}
	

	//uninitialized local variable in use:
	int *z=new int[7];
	for (int i = 0; i < 6; i++)
	{
		z[i] = i;
	}
	for (int i = 0; i < 6; i++)
	{
		cout << z[i];
	}
}


